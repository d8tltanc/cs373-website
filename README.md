# CS373: Software Engineering IDB Project

## Members
* William Li
  * EID: wzl62
  * Gitlab ID: williamli1
* Bennett Ramirez
  * EID: bar2954
  * GitLab ID: oldmud0
* Spencer Sharp (project leader)
  * EID: sps2624
  * GitLab ID: SpencerSharp
* Cheng Tan
  * EID: ct28329
  * Gitlab ID: d8tltanc
* Michael Yu
  * EID: smy447
  * Gitlab ID: mi-yu1

## GitLab info
* Git SHA: c0f36114c87c20b5839c34f93be939844c7f82ec
* GitLab Pipelines: https://gitlab.com/oldmud0/cs373-website/pipelines

## Completion times

### Phase I

* William Li
  * Estimated completion time: 8
  * Actual completion time: 5
* Bennett Ramirez
  * Estimated completion time: 8
  * Actual completion time: 6.5
* Spencer Sharp
  * Estimated completion time: 10
  * Actual completion time: 11.5
* Cheng Tan
  * Estimated completion time: 6
  * Actual completion time: 5
* Michael Yu
  * Estimated completion time: 5
  * Actual completion time: 9

### Phase II

* William Li
  * Estimated completion time: 
  * Actual completion time: 
* Bennett Ramirez
  * Estimated completion time: 
  * Actual completion time: 
* Spencer Sharp
  * Estimated completion time: 12
  * Actual completion time: 1.75
* Cheng Tan
  * Estimated completion time: 15 
  * Actual completion time: 20
* Michael Yu
  * Estimated completion time: 8
  * Actual completion time: 

## Comments

Currently, there is no formal GitLab pipeline since the backend has not been
made yet. Frontend deployment is performed automatically through Netlify.
