import requests
import time
import csv
import json
import os

symbols = []

with open('sp500.csv') as csvfile:
    sp500 = csv.reader(csvfile, delimiter=',')
    next(sp500, None) # skip headers

    for row in sp500:
        symbols.append(row)


baseurl = "https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v2/get-summary"

headers = {
    'x-rapidapi-host': "apidojo-yahoo-finance-v1.p.rapidapi.com",
    'x-rapidapi-key': os.environ.get('YAHOO_API_KEY')
}

responses = []

def call_api(sym, name):
    print('Scraping ' + sym + ' (' + name + ')...', end='', flush=True)
    querystring = {"region": "US", "symbol": sym}
    response = requests.request("GET", baseurl, headers=headers, params=querystring)

    if response.status_code == 200:
        responses.append(response.json())
        print('done')
    else:
        print('\nThere was an error with the request:\n\t' + response.text)

for sym, name, sector in symbols:
    try:
        call_api(sym, name)
        time.sleep(0.5)
    except Exception as e:
        print('\nSomething went wrong:')
        print(e)

# output file
with open('yahoo_scrape.json', 'w') as out:
    json.dump(responses, out)
