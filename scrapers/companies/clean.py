import json

def clean_company(c):
    company = {
        "id": c["quoteType"]["uuid"],
        "name": c["quoteType"]["shortName"],
        "symbol": c["quoteType"]["symbol"],
        "market_cap": c["price"]["marketCap"]["raw"],
        "esg_scores": c.get("esgScores", {}),
        "summary": c["summaryProfile"]
    }

    return company

with open('yahoo_scrape_with_coords.json') as companies_scrapefile:
    companies = json.load(companies_scrapefile)
    cleaned = []

    for c in companies:
        if not c:
            continue
        print(f'Cleaning {c["quoteType"]["shortName"]}')
        try:
            cleaned.append(clean_company(c))
        except Exception as e:
            print(e)

    with open('companies_cleaned.json', 'w') as outfile:
        print(f'Cleaned {len(cleaned)} companies, writing to companies_cleaned.json...')
        json.dump(cleaned, outfile)
