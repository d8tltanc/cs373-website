import json
from uuid import uuid4, UUID

class UUIDEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID):
            # if the obj is uuid, we simply return the value of uuid
            return str(obj)
        return json.JSONEncoder.default(self, obj)

operator_idx = {}

with open('operators.json') as operators_file:
    operator_idx = json.load(operators_file)

KEYS_TO_DELETE = [
    "created_or_updated_in_changeset_id",
    "created_at",
    "updated_at",
    "color",
    "operated_by_name"
]

def clean_route(r):
    r["id"] = uuid4()
    for k in KEYS_TO_DELETE:
        del r[k]

    r["operator"] = operator_idx[r["operated_by_onestop_id"]]
    return r

with open('routes.json') as routes_scrapefile:
    routes = json.load(routes_scrapefile)
    cleaned = []

    for r in routes:
        if not r:
            continue
        print(f'Cleaning {r["name"]} ({r["operated_by_name"]})')
        try:
            cleaned.append(clean_route(r))
        except Exception as e:
            print(e)

    with open('routes_cleaned.json', 'w') as outfile:
        json.dump(cleaned, outfile, cls=UUIDEncoder)

    with open('route_single.json', 'w') as outfile:
        json.dump(cleaned[0], outfile, cls=UUIDEncoder)
