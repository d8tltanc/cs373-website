import requests
import json
from time import sleep

with open('../cities/cities_scrape.json') as cities_file:
    cities = json.load(cities_file)
    operators = {}

    for city in cities:
        if not city:
            continue

        try:
            print(f'Getting routes for {city["name"]}...', end='', flush=True)
            latitude = city["location"]["latlon"]["latitude"]
            longitude = city["location"]["latlon"]["longitude"]

            resp = requests.get('https://transit.land/api/v1/operators?lon=' + str(longitude) + '&lat=' + str(latitude) + '&r=' + str(2000))
            data = resp.json()
            for operator in data["operators"]:
                if operator["onestop_id"] not in operators:
                    operators[operator["onestop_id"]] = operator

            print(f'done (found {len(data["operators"])} operators).', flush=True)
        except Exception as e:
            print('error:')
            print(e)
            print(city)

        sleep(0.75)

    with open('operators.json', 'w') as operators_outfile:
        json.dump(operators, operators_outfile)

    route_url = 'https://transit.land/api/v1/routes?operated_by='

    routes = []
    for operator in operators.values():
        try:
            print(f'Getting routes for {operator["onestop_id"]} ({operator["name"]})...', end='', flush=True)
            operator_id = operator["onestop_id"]
            route_resp = requests.get(route_url + operator_id)
            route_data = route_resp.json()

            routes.extend(route_data["routes"])
            print(f'done (found {len(route_data["routes"])} routes).', flush=True)
        except Exception as e:
            print('error:')
            print(e)
            print(operator)

        sleep(0.75)

    with open('routes.json', 'w') as routes_outfile:
        json.dump(routes, routes_outfile)
