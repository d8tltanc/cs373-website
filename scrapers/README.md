## Prerequisites

1. Install requirements

```
pip install -r requirements.txt
```

2. Set API key in shell

```
export YAHOO_API_KEY=<your key here>
```

### Yahoo Finance scraper

1. Run the script in `yahoo/scrape_yahoo.py`, output will be in `yahoo_scrape.json`

```
python3 yahoo/yahoo_scrape.py
```

### City data scraper

1. Run the script in `cities/scrape.py`, output will be in cities_scrape.json

```
python3 cities/scrape.py
```

### Transit scraper
This scraper runs in two parts, first it scrapes operator data, then it  scrapes route data for each operator.

1. Run the script in `transit/scrape.py`, output will be in operators.json and routes.json.

```
python3 transit/scrape.py
```