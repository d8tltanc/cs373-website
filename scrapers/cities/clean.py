import json
from uuid import uuid4, UUID

class UUIDEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID):
            # if the obj is uuid, we simply return the value of uuid
            return str(obj)
        return json.JSONEncoder.default(self, obj)

KEYS_TO_DELETE = [
    "_links",
    "geoname_id"
]

def clean_city(city):
    # generate a UUID
    city["id"] = uuid4()

    for k in KEYS_TO_DELETE:
        del city[k]

    scores = city["scores"]
    del scores["_links"]

    city["summary"] = scores["summary"]

    # metrics cleaning
    city["overall_score"] = scores["teleport_city_score"]
    city["metrics"] = []
    for category in scores["categories"]:
        city["metrics"].append({
            "name": category["name"],
            "score_out_of_10": category["score_out_of_10"]
        })
    del city["scores"]

    # clean up latitude/longitude info
    city["latlon"] = city["location"]["latlon"]
    del city["location"]

    return city

with open('cities_scrape.json') as cities_scrapefile:
    cities = json.load(cities_scrapefile)

    cleaned = []
    for c in cities:
        if not c:
            continue
        print(f'Cleaning {c["full_name"]}')
        try:
            cleaned.append(clean_city(c))
        except Exception as e:
            print(e)

    with open('cities_clean.json', 'w') as outfile:
        json.dump(cleaned, outfile, cls=UUIDEncoder)

        print(f'Cleaned {len(cleaned)} cities')

