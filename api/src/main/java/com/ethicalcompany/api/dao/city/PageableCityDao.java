package com.ethicalcompany.api.dao.city;

import com.ethicalcompany.api.domain.city.City;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository("cityDao")
public interface PageableCityDao extends PagingAndSortingRepository<City, String> {

}
