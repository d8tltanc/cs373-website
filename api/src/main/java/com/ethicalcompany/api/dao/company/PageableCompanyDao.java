package com.ethicalcompany.api.dao.company;

import com.ethicalcompany.api.domain.company.Company;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository("companyDao")
public interface PageableCompanyDao extends PagingAndSortingRepository<Company, String> {

}
