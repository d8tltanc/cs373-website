package com.ethicalcompany.api.dao.route;

import com.ethicalcompany.api.domain.route.Route;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository("routeDao")
public interface PageableRouteDao extends PagingAndSortingRepository<Route, String> {

}