package com.ethicalcompany.api.service.city;

import com.ethicalcompany.api.domain.city.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface CityService {
    Page<City> findAllCity(Pageable pageable);
    Optional<City> findCityById(String id);
}
