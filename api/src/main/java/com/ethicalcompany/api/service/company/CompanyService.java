package com.ethicalcompany.api.service.company;

import com.ethicalcompany.api.domain.company.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface CompanyService {
    Page<Company> findAllCompany(Pageable pageable);
    Optional<Company> findCompanyById(String id);
}
