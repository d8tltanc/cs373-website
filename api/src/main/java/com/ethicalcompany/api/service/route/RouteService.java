package com.ethicalcompany.api.service.route;

import com.ethicalcompany.api.domain.route.Route;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface RouteService {
    Page<Route> findAllRoute(Pageable pageable);
    Optional<Route> findRouteById(String id);
}
