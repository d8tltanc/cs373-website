package com.ethicalcompany.api.service.company;

import com.ethicalcompany.api.dao.company.PageableCompanyDao;
import com.ethicalcompany.api.domain.company.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("companyService")
public class CompanyServiceImpl implements CompanyService{
    @Autowired
    private PageableCompanyDao pageableCompanyDao;

    @Override
    public Page<Company> findAllCompany(Pageable pageable) {
        return pageableCompanyDao.findAll(pageable);
    }

    @Override
    public Optional<Company> findCompanyById(String id) {
        return pageableCompanyDao.findById(id);
    }

}
