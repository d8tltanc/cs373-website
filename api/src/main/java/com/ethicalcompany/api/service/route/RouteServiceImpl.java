package com.ethicalcompany.api.service.route;

import com.ethicalcompany.api.dao.route.PageableRouteDao;
import com.ethicalcompany.api.domain.route.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("routeService")
public class RouteServiceImpl implements RouteService{
    @Autowired
    private PageableRouteDao pageableRouteDao;

    @Override
    public Page<Route> findAllRoute(Pageable pageable) {
        return pageableRouteDao.findAll(pageable);
    }

    @Override
    public Optional<Route> findRouteById(String id) {
        return pageableRouteDao.findById(id);
    }

}
