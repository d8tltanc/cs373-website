package com.ethicalcompany.api.controller.company;

import java.util.List;
import java.util.Optional;

import com.ethicalcompany.api.domain.company.Company;
import com.ethicalcompany.api.domain.company.CompanyEndpointInfo;
import com.ethicalcompany.api.service.company.CompanyService;
import com.ethicalcompany.api.service.company.CompanyServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CompanyController {
    @Autowired
    private CompanyService companyService;

    @RequestMapping("/v1/company/list")
    public Page<Company> getCompanyList(
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "15") Integer size) {
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable = new PageRequest(page, size, sort);
        return companyService.findAllCompany(pageable);
    }

    @RequestMapping("/v1/company/detail")
    public Optional<Company> getCompanyDetail(
            @RequestParam(value="id", defaultValue = "1") String id) {
        return companyService.findCompanyById(id);
    }

    @RequestMapping("/v1/company/info")
    public CompanyEndpointInfo getInfo() {
        return new CompanyEndpointInfo();
    }
}
