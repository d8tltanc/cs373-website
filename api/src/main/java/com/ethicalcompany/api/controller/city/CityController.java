package com.ethicalcompany.api.controller.city;
import com.ethicalcompany.api.domain.city.City;
import com.ethicalcompany.api.domain.city.CityEndpointInfo;
import com.ethicalcompany.api.domain.company.Company;
import com.ethicalcompany.api.service.city.CityService;
import com.ethicalcompany.api.service.company.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@CrossOrigin
public class CityController {
    @Autowired
    private CityService cityService;

    @RequestMapping("/v1/city/list")
    public Page<City> getCompanyList(
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "15") Integer size) {
        Sort sort = new Sort(Sort.Direction.DESC, "id");

        Pageable pageable = new PageRequest(page, size, sort);
        return cityService.findAllCity(pageable);
    }

    @RequestMapping("/v1/city/detail")
    public Optional<City> getCompanyDetail(
            @RequestParam(value="id", defaultValue = "1") String id) {
        return cityService.findCityById(id);
    }

    @RequestMapping("/v1/city/info")
    public CityEndpointInfo getInfo() {
        return new CityEndpointInfo();
    }
}
