package com.ethicalcompany.api.controller.route;

import java.util.Optional;

import com.ethicalcompany.api.domain.route.Route;
import com.ethicalcompany.api.domain.route.RouteEndpointInfo;
import com.ethicalcompany.api.service.route.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class RouteController {
    @Autowired
    private RouteService routeService;

    @RequestMapping("/v1/route/list")
    public Page<Route> getRouteList(
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "15") Integer size) {
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable = new PageRequest(page, size, sort);
        return routeService.findAllRoute(pageable);
    }

    @RequestMapping("/v1/route/detail")
    public Optional<Route> getRouteDetail(
            @RequestParam(value="id", defaultValue = "1") String id) {
        return routeService.findRouteById(id);
    }

    @RequestMapping("/v1/route/info")
    public RouteEndpointInfo getInfo() {
        return new RouteEndpointInfo();
    }
}
