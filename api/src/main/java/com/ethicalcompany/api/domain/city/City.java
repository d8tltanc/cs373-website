package com.ethicalcompany.api.domain.city;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cities")
public class City {
    @Id
    @Column(name="id")
    private String id;
    @Column(name="name")
    private String name;
    @Column(name="population")
    private Integer population;
    @Column(name="summary")
    private String summary;
    @Column(name="lation")
    private String lation;
    @Column(name="metrics")
    private String matrics;
    @Column(name="overallscore")
    private Double overallScore;
    @Column(name="full_name")
    private String fullName;
    @Column(name="country")
    private String country;
    @Column(name="state")
    private String state;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPopulation() {
        return population;
    }

    public String getSummary() {
        return summary;
    }

    public String getLation() {
        return lation;
    }

    public String getMatrics() {
        return matrics;
    }

    public Double getOverallScore() {
        return overallScore;
    }

    public String getFullName() {
        return fullName;
    }

    public String getCountry() {
        return country;
    }

    public String getState() {
        return state;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setLation(String lation) {
        this.lation = lation;
    }

    public void setMatrics(String matrics) {
        this.matrics = matrics;
    }

    public void setOverallScore(Double overallScore) {
        this.overallScore = overallScore;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "City{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", population=" + population +
                ", summary='" + summary + '\'' +
                ", lation='" + lation + '\'' +
                ", matrics='" + matrics + '\'' +
                ", overallScore=" + overallScore +
                ", fullName='" + fullName + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
