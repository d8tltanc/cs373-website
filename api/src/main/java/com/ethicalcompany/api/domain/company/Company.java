package com.ethicalcompany.api.domain.company;

import com.ethicalcompany.api.datatype.Location;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="companies")
public class Company implements Serializable {
    @Id
    @Column(name="id")
    private String id;
    @Column(name="address")
    private String address;
    @Column(name="location")
    private String location;
    @Column(name="city_id")
    private String city_id;
    @Column(name="website")
    private String website;
    @Column(name="industry")
    private String industry;
    @Column(name="summary")
    private String summary;
    @Column(name="n_employees")
    private String n_employees;
    @Column(name="name")
    private String name;
    @Column(name="market_cap")
    private long market_cap;
    @Column(name="symbol")
    private String symbol;

    public String getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getLocation() {
        return location;
    }

    public String getCity_id() {
        return city_id;
    }

    public String getWebsite() {
        return website;
    }

    public String getIndustry() {
        return industry;
    }

    public String getSummary() {
        return summary;
    }

    public String getN_employees() {
        return n_employees;
    }

    public String getName() {
        return name;
    }

    public long getMarket_cap() {
        return market_cap;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setN_employees(String n_employees) {
        this.n_employees = n_employees;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMarket_cap(long market_cap) {
        this.market_cap = market_cap;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id='" + id + '\'' +
                ", address='" + address + '\'' +
                ", location=" + location +
                ", city_id='" + city_id + '\'' +
                ", website='" + website + '\'' +
                ", industry='" + industry + '\'' +
                ", summary='" + summary + '\'' +
                ", n_employees='" + n_employees + '\'' +
                ", name='" + name + '\'' +
                ", market_cap=" + market_cap +
                ", symbol='" + symbol + '\'' +
                '}';
    }
}
