package com.ethicalcompany.api.domain.company;

import java.time.LocalDateTime;

public class CompanyEndpointInfo {
    public CompanyEndpointInfo(){
    }
    public LocalDateTime getServerTime() {
        return LocalDateTime.now();
    }
}
