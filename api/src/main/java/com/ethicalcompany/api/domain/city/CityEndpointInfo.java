package com.ethicalcompany.api.domain.city;

import java.time.LocalDateTime;

public class CityEndpointInfo {

    public CityEndpointInfo(){
    }

    public LocalDateTime getServerTime() {
        return LocalDateTime.now();
    }
}
