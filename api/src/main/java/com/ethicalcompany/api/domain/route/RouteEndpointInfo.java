package com.ethicalcompany.api.domain.route;

import java.time.LocalDateTime;

public class RouteEndpointInfo {
    public RouteEndpointInfo(){
    }
    public LocalDateTime getServerTime() {
        return LocalDateTime.now();
    }
}
