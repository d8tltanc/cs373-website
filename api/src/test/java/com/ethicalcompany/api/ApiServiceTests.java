package com.ethicalcompany.api;

import com.ethicalcompany.api.domain.city.City;
import com.ethicalcompany.api.domain.company.Company;
import com.ethicalcompany.api.domain.route.Route;
import com.ethicalcompany.api.service.city.CityService;
import com.ethicalcompany.api.service.company.CompanyService;
import com.ethicalcompany.api.service.route.RouteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApiServiceTests {
    @Autowired
    private CompanyService companyService;
    @Autowired
    private CityService cityService;
    @Autowired
    private RouteService routeService;

    @Test
    public void testCompanyService() {
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable = new PageRequest(2, 8, sort);
        Page<Company> companyPage = companyService.findAllCompany(pageable);
        List<Company> companyList = companyPage.getContent();

        assert !companyPage.isEmpty();
        assert companyList.size() == 8;

        String companyId = companyList.get(0).getId();
        Optional<Company> company = companyService.findCompanyById(companyId);
        assert company.isPresent();
    }

    @Test
    public void testCityService() {
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable = new PageRequest(2, 8, sort);
        Page<City> cityPage = cityService.findAllCity(pageable);
        List<City> cityList = cityPage.getContent();

        assert !cityPage.isEmpty();
        assert cityList.size() == 8;

        String cityId = cityList.get(0).getId();
        Optional<City> city = cityService.findCityById(cityId);
        assert city.isPresent();
    }

    @Test
    public void testRouteService() {
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable = new PageRequest(2, 8, sort);
        Page<Route> routePage = routeService.findAllRoute(pageable);
        List<Route> routeList = routePage.getContent();

        assert !routePage.isEmpty();
        assert routeList.size() == 8;

        String routeId = routeList.get(0).getId();
        Optional<Route> route = routeService.findRouteById(routeId);
        assert route.isPresent();
    }
}
