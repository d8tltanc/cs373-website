/**
 * Wrapper for ES6 fetch method. Example usage:
 *
 *      import request from './utils/request';
 *      try {
 *          const data = await request('https://api.example.com');
 *      } catch(e) {
 *          console.log(e.status);
 *      }
 *
 * @param url request url
 * @param opts see https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/fetch
 */
const request = async (url: string, opts?: object) => {
  const response = await fetch(url, opts);
  const json = await response.json();

  if (response.ok) { return json; }

  const error = {
    ...json,
    status: response.status,
  };

  return error;
};

export default request;
