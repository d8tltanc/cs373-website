import { Coordinate } from './City';

type ESGScoreDetail = {
  min: number;
  avg: number;
  max: number;
}

type ESGScoreObject = {
  palmOil: boolean;
  controversialWeapons: boolean;
  gambling: boolean;
  nuclear: boolean;
  alcoholic: boolean;
  gmo: boolean;
  animalTesting: boolean;
  tobacco: boolean;
  coal: boolean;
  pesticides: boolean;
  militaryContract: boolean;
  totalEsg: number

  highestControversy: number;
  peerHighestControversyPerformance: ESGScoreDetail;

  esgPerformance: string;
  peerEsgScorePerformance: ESGScoreDetail;

  socialScore: number;
  peerSocialPerformance: ESGScoreDetail;

  environmentScore: number;
  peerEnvironmentPerformance: ESGScoreDetail;

  governanceScore: number;
  peerGovernancePerformance: ESGScoreDetail;
}

type Company = {
  id: string;
  address: string;
  location: Coordinate;
  cityId: string;
  stockPrice: number;
  income: number;
  logoUrl: string;
  industry: string;
  esgScores: ESGScoreObject;
};

export default Company;
