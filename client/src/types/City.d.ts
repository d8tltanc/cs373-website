type CityMetric = {
  category: string;
  scoreOutOf10: number;
};

type Coordinate = {
  latitude: number;
  longitude: number;
};

type City = {
  id: string;
  name: string;
  country: string;
  latlon: Coordinate;
  population: number;
  metrics: CityMetric[];
  summary: string;
  overallScore: number;
  companies: string[]; // array of company ids
  transitRoutes: string[]; // array of transit route ids
};

export { Coordinate };
export default City;
