type Point = {
  x: number;
  y: number;
};

type TransitStop = {
  id: string;
  name: string;
};

type TransitRoute = {
  id: string;
  cityId: string;
  onestopId: string;
  name: string;
  vehicleType: string;
  stops: TransitStop[];
  bikesAllowed: string;
  wheelchairAccessible: string;
  operator: string;
  operatorUrl: string;
  geometry: Point[];
}

export default TransitRoute;
