import request from '../../utils/request';

const mock = jest.spyOn(window, 'fetch') as jest.Mock;

describe('Fetch wrapper', () => {
  it('returns data on sucessful request', async () => {
    mock.mockImplementationOnce((url: string) => Promise.resolve({
      json: () => ({ value: url }),
      ok: true,
    }));

    const data = await request('testurl');

    expect(data).toEqual({ value: 'testurl' });
  });

  it('throws error on bad request', async () => {
    mock.mockImplementationOnce((url: string) => Promise.resolve({
      json: () => ({}),
      ok: false,
      status: 400,
    }));

    const data = await request('testurl');

    expect(data).toEqual({ status: 400 });
  });
});
