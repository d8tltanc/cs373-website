import styled from 'styled-components';

export const DirectoryTableWrapper = styled.div`
  max-height: 75vh;
`;

export const LinkedRow = styled.tr`
  cursor: pointer;
`;
