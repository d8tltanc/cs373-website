import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import Navbar from './components/Navbar';
import HomePage from './pages/HomePage';
import AboutPage from './pages/AboutPage';
import CityDirectory from './pages/directories/CityDirectory';
import CompanyDirectory from './pages/directories/CompanyDirectory';
import TransitRouteDirectory from './pages/directories/TransitRouteDirectory';
import CityInstance from './pages/instances/CityInstance';
import CompanyInstance from './pages/instances/CompanyInstance';
import TransitRouteInstance from './pages/instances/TransitRouteInstance';
import NotFoundPage from './pages/NotFoundPage';

class App extends React.Component {
  public render() {
    return (
      <Router>
        <Navbar />
        <Container>
          <Switch>
            <Route path="/" exact>
              <HomePage />
            </Route>
            <Route path="/about" exact>
              <AboutPage />
            </Route>
            <Route path="/cities" exact>
              <CityDirectory />
            </Route>
            <Route path="/companies" exact>
              <CompanyDirectory />
            </Route>
            <Route path="/transit" exact>
              <TransitRouteDirectory />
            </Route>
            <Route path="/cities/cityinstance" exact>
              <CityInstance />
            </Route>
            <Route path="/companies/companyinstance" exact>
              <CompanyInstance />
            </Route>
            <Route path="/transit/transitrouteinstance" exact>
              <TransitRouteInstance />
            </Route>
            <Route>
              <NotFoundPage />
            </Route>
          </Switch>
        </Container>
      </Router>
    );
  }
}

export default App;
