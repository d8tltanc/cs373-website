import React from 'react';
import {LinkedRow} from '../../components/Constants/Constants'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Table from 'react-bootstrap/Table';

class CompanyInstance extends React.Component{
    render() {
    return (
      <Container>
        <Row className="m-3">
            <Col><h1>Company Name</h1></Col>
        </Row>
        <Row className="m-3">
            <Col>
                <Container className="p-3 border rounded-lg bg-light">
                   <h3>Summary</h3>
                   <p></p> 
                </Container>
            </Col>
        </Row>
        <Row className="m-3">
            <Col>
                <Container className="p-3 border rounded-lg bg-light">
                    <h3>Company Info</h3>
                    <Table>
                        <tr>
                            <th>Website:</th>
                            <td></td>
                        </tr>
                        <tr>
                            <th>Address:</th>
                            <td></td>
                        </tr>
                        <tr>
                            <th>Phone Number:</th>
                            <td></td>
                        </tr>
                        <tr>
                            <th>Industry:</th>
                            <td></td>
                        </tr>
                        <tr>
                            <th>Full Time Employees:</th>
                            <td></td>
                        </tr>
                    </Table>
                </Container>
            </Col>
        </Row>
        <Row className="m-3">
            <Col>
                <Container className="p-3 border rounded-lg bg-light">
                <h3>ESG Score</h3>
                <Row>
                    <Col>
                        <Table>
                            <tr>
                                <td>Palm Oil:</td>
                                <td><input className="ml-3 align-self-center" type="checkbox" disabled/></td>
                            </tr>
                            <tr>
                                <td>Controversial Weapons:</td>
                                <td><input className="ml-3 align-self-center" type="checkbox" disabled/></td>
                            </tr>
                            <tr>
                                <td>Gambling:</td>
                                <td><input className="ml-3 align-self-center" type="checkbox" disabled/></td>
                            </tr>
                            <tr>
                                <td>Nuclear:</td>
                                <td><input className="ml-3 align-self-center" type="checkbox" disabled/></td>
                            </tr>
                            <tr>
                                <td>Fur/Leather:</td>
                                <td><input className="ml-3 align-self-center" type="checkbox" disabled/></td>
                            </tr>
                            <tr>
                                <td>Alcoholic:</td>
                                <td><input className="ml-3 align-self-center" type="checkbox" disabled/></td>
                            </tr>
                            <tr>
                                <td>GMO:</td>
                                <td><input className="ml-3 align-self-center" type="checkbox" disabled/></td>
                            </tr>
                            <tr>
                                <td>Catholic:</td>
                                <td><input className="ml-3 align-self-center" type="checkbox" disabled/></td>
                            </tr>
                            <tr>
                                <td>Animal Testing:</td>
                                <td><input className="ml-3 align-self-center" type="checkbox" disabled/></td>
                            </tr>
                            <tr>
                                <td>Tobacco:</td>
                                <td><input className="ml-3 align-self-center" type="checkbox" disabled/></td>
                            </tr>
                            <tr>
                                <td>Coal:</td>
                                <td><input className="ml-3 align-self-center" type="checkbox" disabled/></td>
                            </tr>
                            <tr>
                                <td>Adult:</td>
                                <td><input className="ml-3 align-self-center" type="checkbox" disabled/></td>
                            </tr>
                            <tr>
                                <td>Pesticides:</td>
                                <td><input className="ml-3 align-self-center" type="checkbox" disabled/></td>
                            </tr>
                            <tr>
                                <td>Small Arms:</td>
                                <td><input className="ml-3 align-self-center" type="checkbox" disabled/></td>
                            </tr>
                            <tr>
                                <td>Military Contract:</td>
                                <td><input className="ml-3 align-self-center" type="checkbox" disabled/></td>
                            </tr>
                        </Table>
                    </Col>
                    <Col>
                        <Table>
                            <tr>
                                <th><h5>Total ESG:</h5></th>
                                <td></td>
                            </tr>
                            <tr>
                                <th><h5>Social Score:</h5></th>
                                <td></td>
                            </tr>
                            <tr>
                                <th><h5>Environment Score:</h5></th>
                                <td></td>
                            </tr>
                            <tr>
                                <th><h5>Governance Score:</h5></th>
                                <td></td>
                            </tr>
                        </Table>
                    </Col>
                </Row>
            </Container>
            </Col>
        </Row>
      </Container>
    );
  }
}

export default CompanyInstance;

