import React from 'react';
import {LinkedRow} from '../../components/Constants/Constants'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Table from 'react-bootstrap/Table';

class CityInstance extends React.Component{
    render() {
    return (
      <Container>
        <Row className="m-3">
            <Col><h1>City Name</h1></Col>
        </Row>
        <Row className="m-3">
            <Col>
                <Container className="p-3 border rounded-lg bg-light">
                    <h3>Basic Info</h3>
                    <Table>
                        <tr>
                            <th>Country:</th>
                            <td></td>
                        </tr>
                        <tr>
                            <th>Location:</th>
                            <td></td>
                        </tr>
                        <tr>
                            <th>Population:</th>
                            <td></td>
                        </tr>
                        <tr>
                            <th>Overall Score:</th>
                            <td></td>
                        </tr>
                    </Table>
                </Container>
                <Container className="p-3 mt-3 border rounded-lg bg-light overflow-auto" style={{minHeight:'220px', maxHeight:"220px"}}>
                    <h3>Summary</h3>
                    <p></p>
                </Container>
            </Col>
            <Col>
                <Container className="p-3 border rounded-lg bg-light">
                    <h3>Metrics</h3>
                    <Container className="border rounded-lg overflow-auto bg-white" style={{maxHeight:"445px"}}>
                        <Table>
                            <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Score out of 10</th>
                                </tr>
                            </thead>
                        </Table>
                    </Container>
                </Container>
            </Col>
        </Row>
        <Row className="m-3">
            <Col>
                <Container className="p-3 border rounded-lg bg-light">
                    <h3>Companies</h3>
                    <Container className="border rounded-lg overflow-auto bg-white" style={{maxHeight:"330px"}}>
                        <Table>
                            <thead>
                                <tr>
                                    <th>Company Name</th>
                                    <th>Total ESG</th>
                                </tr>
                            </thead>
                        </Table>
                    </Container>
                </Container>
            </Col>
            <Col>
                <Container className="p-3 border rounded-lg bg-light">
                    <h3>Transit Routes</h3>
                    <Container className="border rounded-lg overflow-auto bg-white" style={{maxHeight:"330px"}}>
                        <Table>
                        </Table>
                    </Container>
                </Container>
            </Col>
        </Row>
      </Container>
    );
  }
}

export default CityInstance;

