import React from 'react';
import {LinkedRow} from '../../components/Constants/Constants'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Table from 'react-bootstrap/Table';

class TransitRouteInstance extends React.Component{
    render() {
    return (
      <Container>
        <Row className="m-3">
            <Col><h1>Transit Route Name</h1></Col>
        </Row>
        <Row className="m-3">
            <Col>
                <Container className="p-3 border rounded-lg bg-light">
                    <h3>Information</h3>
                        <Table>
                            <tr>
                                <td>City:</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Vehicle:</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Bikes Allowed:</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Operator URL:</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Operator Phone Number:</td>
                                <td></td>
                            </tr>
                        </Table>
                </Container>
            </Col>
            <Col>
                <Container className="p-3 border rounded-lg bg-light">
                    <h3>Stops</h3>
                    <Container className="border rounded-lg overflow-auto bg-white" style={{maxHeight:"260px"}}>
                        <Table>
                        </Table>
                    </Container>
                </Container>
            </Col>
        </Row>
      </Container>
    );
  }
}

export default TransitRouteInstance;

