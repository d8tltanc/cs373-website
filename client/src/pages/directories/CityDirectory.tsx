import React from 'react';
import {DirectoryTableWrapper, LinkedRow} from '../../components/Constants/Constants'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Table from 'react-bootstrap/Table';

class CityDirectory extends React.Component{
	render() {
    return (
      <Container>
      	<Row className="m-3">
      		<Col><h1>City Directory</h1></Col>
      	</Row>
      	<Row className="m-3">
      		<Col>
      			<Container className="p-3 border rounded-lg bg-light">
      				<DirectoryTableWrapper className="border rounded-lg overflow-auto bg-white">
      					<Table className="table-hover">
	      					<thead>
			                  	<tr>
			                    	<th>City Name</th>
			                    	<th>Overall Score</th>
			                  	</tr>
			                </thead>
			                <tbody>
		                		<LinkedRow>
		                			<td>City</td>
		                			<td>Score</td>
			                	</LinkedRow>
			                </tbody>
	      				</Table>
      				</DirectoryTableWrapper>
      			</Container>
      		</Col>
      	</Row>
      </Container>
    );
  }
}

export default CityDirectory;

