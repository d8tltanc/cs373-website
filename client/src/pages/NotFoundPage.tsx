import React from 'react';

const NotFoundPage: React.FunctionComponent = () => (
  <h1>Page not found</h1>
);

export default NotFoundPage;
